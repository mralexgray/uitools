#import <MobileCoreServices/MobileCoreServices.h>

extern CFArrayRef SBSCopyApplicationDisplayIdentifiers(bool active,bool debuggable);
extern CFStringRef SBSCopyFrontmostApplicationDisplayIdentifier();
extern bool SBSProcessIDForDisplayIdentifier(CFStringRef identifier,pid_t* pid);
extern int SBSLaunchApplicationForDebugging(CFStringRef identifier,CFURLRef URL,CFArrayRef args,CFDictionaryRef env,CFStringRef stdout,CFStringRef stderr,char flags);
extern CFStringRef SBSApplicationLaunchingErrorString(int error);
extern void SBSOpenSensitiveURLAndUnlock(CFURLRef URL,bool unlock);

static LSApplicationProxy* $_getAppProxy(CFStringRef key) {
  if(!key){return nil;}
  LSApplicationProxy* appProxy=nil;
  CFArrayRef identifiers=SBSCopyApplicationDisplayIdentifiers(false,false);
  for (NSString* identifier in (NSArray*)identifiers){
    LSApplicationProxy* proxy=[LSApplicationProxy applicationProxyForIdentifier:identifier];
    NSString* name=proxy.localizedName;
    if(CFStringFindWithOptions((CFStringRef)name,key,CFRangeMake(0,name.length),
     kCFCompareCaseInsensitive|kCFCompareDiacriticInsensitive,NULL)){
      appProxy=proxy;
      break;
    }
  }
  CFRelease(identifiers);
  return appProxy?:[LSApplicationProxy
   applicationProxyForIdentifier:(NSString*)key];
}
struct item {
  NSString* key;
  NSString* identifier;
};
static int $_compareItems(const void* A,const void* B) {
  return CFStringCompare(*(CFStringRef*)A,*(CFStringRef*)B,
   kCFCompareCaseInsensitive|kCFCompareDiacriticInsensitive);
}

int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  const char* $outFile=NULL;
  const char* $errFile=NULL;
  const char* $openURL=NULL;
  int opt;
  while((opt=getopt(argc,argv,"lqo:e:u:"))!=-1){
    if(opt=='l'){
      CFArrayRef identifiers=SBSCopyApplicationDisplayIdentifiers(false,false);
      CFIndex count=CFArrayGetCount(identifiers),i=0;
      unsigned long maxlen=0;
      struct item* list=malloc(sizeof(struct item)*count);
      for (NSString* identifier in (NSArray*)identifiers){
        unsigned long len=identifier.length;
        if(len>maxlen){maxlen=len;}
        LSApplicationProxy* proxy=[LSApplicationProxy applicationProxyForIdentifier:identifier];
        list[i].key=proxy.localizedName;
        list[i].identifier=identifier;
        i++;
      }
      qsort(list,count,sizeof(struct item),$_compareItems);
      for (i=0;i<count;i++){
        NSString* identifier=list[i].identifier;
        fputs(identifier.UTF8String,stdout);
        unsigned long nspace=maxlen-identifier.length,j;
        for (j=0;j<nspace;j++){fputc(' ',stdout);}
        printf("\t%s\n",list[i].key.UTF8String);
      }
      free(list);
      CFRelease(identifiers);
      return 0;
    }
    else if(opt=='q'){
      CFStringRef key=(optind<argc)?CFStringCreateWithCStringNoCopy(NULL,
       argv[optind],kCFStringEncodingUTF8,kCFAllocatorNull):
       SBSCopyFrontmostApplicationDisplayIdentifier();
      LSApplicationProxy* appProxy=$_getAppProxy(key);
      if(key){CFRelease(key);}
      if(!appProxy){return 1;}
      NSString* identifier=appProxy.applicationIdentifier;
      pid_t pid;
      printf("%d\n%s\t%s\n%s\n%s\n",
       SBSProcessIDForDisplayIdentifier((CFStringRef)identifier,&pid)?pid:-1,
       identifier.UTF8String,appProxy.localizedName.UTF8String,
       appProxy.resourcesDirectoryURL.path.UTF8String,
       appProxy.containerURL.path.UTF8String);
      return 0;
    }
    if(!optarg){return 1;}
    switch(opt){
      case 'o':$outFile=optarg;break;
      case 'e':$errFile=optarg;break;
      case 'u':$openURL=optarg;break;
    }
  }
  opt=optind;
  if(opt<argc){
    LSApplicationProxy* appProxy=nil;
    CFMutableArrayRef args=CFArrayCreateMutable(NULL,0,&kCFTypeArrayCallBacks);
    while(opt<argc){
      CFStringRef arg=CFStringCreateWithCStringNoCopy(NULL,
       argv[opt],kCFStringEncodingUTF8,kCFAllocatorNull);
      if(!arg){continue;}
      if(!appProxy){
        appProxy=$_getAppProxy(arg);
        if(!appProxy){return 1;}
      }
      else {CFArrayAppendValue(args,arg);}
      CFRelease(arg);
      opt++;
    }
    CFStringRef outFile=$outFile?
     CFStringCreateWithFileSystemRepresentation(NULL,$outFile):NULL;
    CFStringRef errFile=$errFile?
     CFStringCreateWithFileSystemRepresentation(NULL,$errFile):
     outFile?CFRetain(outFile):NULL;
    CFURLRef openURL=$openURL?CFURLCreateWithBytes(NULL,(void*)$openURL,
     strlen($openURL),kCFStringEncodingUTF8,NULL):NULL;
    int err=SBSLaunchApplicationForDebugging(
     (CFStringRef)appProxy.applicationIdentifier,
     openURL,args,NULL,outFile,errFile,4);
    if(outFile){CFRelease(outFile);}
    if(errFile){CFRelease(errFile);}
    if(openURL){CFRelease(openURL);}
    CFRelease(args);
    if(err){
      fputs("E: ",stderr);
      CFShow(SBSApplicationLaunchingErrorString(err));
      return err;
    }
  }
  else if($openURL){
    if($outFile){fputs("W: Ignoring option -o\n",stderr);}
    if($errFile){fputs("W: Ignoring option -e\n",stderr);}
    CFURLRef openURL=CFURLCreateWithBytes(NULL,(void*)$openURL,
     strlen($openURL),kCFStringEncodingUTF8,NULL);
    if(!openURL){return 1;}
    SBSOpenSensitiveURLAndUnlock(openURL,true);
    CFRelease(openURL);
  }
  else {
    fprintf(stderr,"Usage: %s [-o stdout] [-e stderr] [-u URL] [appID] [arguments...]\n"
     "       %s [-l] [-q [appID]]\n",argv[0],argv[0]);
    return 1;
  }
  [pool drain];
  return 0;
}
